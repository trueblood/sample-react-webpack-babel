import styleLintPlugin from 'stylelint-webpack-plugin';

module.exports = {
    entry: './src/home.js',
    output: {
        path: __dirname + '/public',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader?-autoprefixer!postcss!sass-loader'
            }
        ]
    },
    plugins: [
        new styleLintPlugin({
            configFile: '.stylelintrc',
            context: 'src',
            files: '**/**/**/*.scss',
            syntax: 'scss',
            failOnError: false,
            quiet: false,
        })
    ],
    watch: true
};