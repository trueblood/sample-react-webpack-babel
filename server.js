import config from './config';
import path from 'path';
import bodyParser from 'body-parser';
import express from 'express';
const app = express();

app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(require('./routes/home'));
app.use(require('./routes/matt'));
app.use(require('./routes/project'));

app.use(express.static('public'));

app.listen(config.port, config.host, () => {
    console.info('Express listening on port', config.port);
});
