import express from 'express';
const server = express();

server.get('/project', (req, res) => {
    res.render('project', {
        pageTitle: 'Project',
        pageId: 'project'
    });
});

module.exports = server;