import express from 'express';
const server = express();

server.get('/matt', (req, res) => {
    res.render('matt', {
        pageTitle: 'Matt',
        pageId: 'matt'
    });
});

module.exports = server;