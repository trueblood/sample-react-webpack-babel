import express from 'express';
const app = express();

app.get('/', (req, res) => {
    res.render('home', {
        pageTitle: 'Home',
        pageId: 'home'
    });
});

module.exports = app;