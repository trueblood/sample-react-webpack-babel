import React from 'react';
import ReactDOM from 'react-dom';

import Matt from './components/Matt';

ReactDOM.render(
  <Matt />,
  document.getElementById('matt-main-container')
);