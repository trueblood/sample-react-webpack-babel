import React from 'react';
import ReactDOM from 'react-dom';

import Project from './components/Project';

ReactDOM.render(
  <Project />,
  document.getElementById('project-main-container')
);