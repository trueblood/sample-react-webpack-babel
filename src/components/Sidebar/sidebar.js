import React from 'react';
require('./sidebar.scss');

const Sidebar = () => {
    return (
        <div className="Sidebar">
            <ul>
                <li><a href="#" title="Test 1">test 1</a></li>
                <li><a href="#" title="Test 2">test 2</a></li>
                <li><a href="#" title="Test 3">test 3</a></li>
                <li><a href="#" title="Test 4">test 4</a></li>
            </ul>
        </div>
    );
};

export default Sidebar;