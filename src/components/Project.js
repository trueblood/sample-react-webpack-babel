import React from 'react';
import Header from './Header/header';
import Sidebar from './Sidebar/sidebar';
import Aside from './Aside/aside';

class Project extends React.Component {
    render() {
        return (
            <div className="Project">
                <Header/>
                <Sidebar/>
                <Aside/>
            </div>
        );
    }
}

export default Project;