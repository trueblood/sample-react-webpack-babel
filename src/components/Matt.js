import React from 'react';
import Header from './Header/header';
import Sidebar from './Sidebar/sidebar';

class Matt extends React.Component {
    render() {
        return (
            <div className="Matt">
                <Header/>
                <Sidebar/>
            </div>
        );
    }
}

export default Matt;