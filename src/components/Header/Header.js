import React from 'react';
require('./header.scss');

const Header = () => {
    return (
        <div className="Header">
            <ul>
                <li><a href="./" title="Home">Home</a></li>
                <li><a href="./matt" title="Matt">Matt</a></li>
                <li><a href="./project" title="Project">Project</a></li>
            </ul>
        </div>
    );
};

export default Header;