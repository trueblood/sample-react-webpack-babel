import React from 'react';
require('./aside.scss');

const Aside = () => {
    return (
        <div className="Aside">
            <ul>
                <li><a href="#" title="Aside 1">Aside 1</a></li>
                <li><a href="#" title="Aside 2">Aside 2</a></li>
            </ul>
        </div>
    );
};

export default Aside;